import React, { Component } from 'react';
import { BodycontrolsComponent } from '../shared/bodycontrols/bodyControls';
import { ConfigurationComponent } from '../shared/configuration/configuration';
import { DriverStateComponent } from '../shared/driverstate/driverState';
import { FirmwareComponent } from '../shared/firmware/firmware';
import { HvacComponent } from '../shared/hvac/hvac';
import { StateOfChargeComponent } from '../shared/stateofcharge/stateOfCharge';
import Drawer from '@material-ui/core/Drawer';
import FastForwardIcon from '@material-ui/icons/FastForward';
import FastRewindIcon from '@material-ui/icons/FastRewind';
import './home-style.css';

export class Home extends Component {

  state = {
    configDetails: true
  }
  render() {
    return (
      <div className="parentgrid">
        <table>
          <tbody>
            <tr>
              <td colSpan="3">
                <div className="maingrid">
                  <div className="hvacgrid">
                    <HvacComponent />
                  </div>
                  <div className="driverstategrid">
                    <DriverStateComponent />
                  </div>
                </div>
                <div className="maingrid">
                  <div className="bodycontrolsgrid">
                    <BodycontrolsComponent />
                  </div>
                  <div className="chargefirmgrid">
                    <div>
                      <StateOfChargeComponent />
                    </div>
                    <div>
                      <FirmwareComponent />
                    </div>
                  </div>
                </div>
              </td>
              <td colSpan="1" style={{ width: "100%", height: "100%", alignItems: "right" }}>
                <div
                  className="sidenavdiv"
                  style={{ height: "100vh", display: "flex", flexDirection: "row" }}>
                  {this.state.configDetails ? <div className="configurationgrid">
                    <ConfigurationComponent />
                  </div> : null}
                  <div>
                    <button onClick={this.sideNavToggle} className="buttonsidenav">
                      {this.state.configDetails ? <span className="textSmall">LESS DETAILS
                        <FastRewindIcon />
                      </span> : null}
                      {!this.state.configDetails ? <span className="textSmall">MORE DETAILS
                        <FastForwardIcon />
                      </span> : null}
                    </button>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div >
    );
  }

  sideNavToggle = () => {
    this.state.configDetails = !this.state.configDetails;
    this.setState({ configDetails: this.state.configDetails });
  }
}