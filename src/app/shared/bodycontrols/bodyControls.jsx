import React, { Component } from 'react';
import './body-controls-style.css'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Chip from '@material-ui/core/Chip';
import Slider from '@material-ui/core/Slider';
import * as firebase from 'firebase';

export class BodycontrolsComponent extends Component {
  state = {
    _doors: false,
    _doorsfl: false,
    _doorsfr: false,
    _doorsbl: false,
    _doorsbr: false,
    _trunk: false,
    _frunk: false,
    _windows: false,
    _windowfl: 100,
    _windowfr: 100,
    _windowbl: 100,
    _windowbr: 100,
  }

  componentDidMount() {
    const rootRef = firebase.database().ref().child('bodycontrols-data');
    rootRef.on('value', (snapshot) => {
      let items = snapshot.val();
      this.setState({
        _doors: items[0].doors,
        _doorsfl: items[0].doorsfl,
        _doorsfr: items[0].doorsfr,
        _doorsbl: items[0].doorsbl,
        _doorsbr: items[0].doorsbr,
        _trunk: items[0].trunk,
        _frunk: items[0].frunk,
        _windows: items[0].windows,
        _windowfl: items[0].windowfl,
        _windowfr: items[0].windowfr,
        _windowbl: items[0].windowbl,
        _windowbr: items[0].windowbr
      })
    });
  }

  render() {
    return (
      <div className="bodycontrol">
        <ExpansionPanel style={{ backgroundColor: "hsl(275, 0%, 20%)" }}
          defaultExpanded={true}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon style={{ color: "hsl(0, 0%, 85%)" }} />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            style={{ backgroundColor: "hsl(275, 0%, 20%)", color: "hsl(0, 0%, 85%)" }}>
            <div style={{ display: "flex", width: "100%", justifyContent: "space-between" }}>
              <p className="textBC">DOORS</p>
              {this.state._doors ? <Chip label="OPEN" onClick={this.doorscloseAll} className="textSmallBC" style={{ backgroundColor: "hsl(0, 100%, 39%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
              {!this.state._doors ? <Chip label="CLOSED" onClick={e => {e.stopPropagation()}} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
              <div></div>
            </div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <table style={{ width: "100%" }}>
              <tbody>
                <tr>
                  <td>
                    <p className="textSmallBC">Front L</p>
                  </td>
                  <td>
                    <p className="textSmallBC">Front R</p>
                  </td>
                  <td>
                    <p className="textSmallBC">Back L</p>
                  </td>
                  <td>
                    <p className="textSmallBC">Back R</p>
                  </td>
                  <td>
                    <p className="textSmallBC">Trunk</p>
                  </td>
                  <td>
                    <p className="textSmallBC">Frunk</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label className="switch">
                      <input type="checkbox" checked={this.state._doorsfl} onChange={this.doorFlChange} />
                      <span className="sliderbc round"></span>
                    </label>
                  </td>
                  <td>
                    <label className="switch">
                      <input type="checkbox" checked={this.state._doorsfr} onChange={this.doorFrChange} />
                      <span className="sliderbc round"></span>
                    </label>
                  </td>
                  <td>
                    <label className="switch">
                      <input type="checkbox" checked={this.state._doorsbl} onChange={this.doorBlChange} />
                      <span className="sliderbc round"></span>
                    </label>
                  </td>
                  <td>
                    <label className="switch">
                      <input type="checkbox" checked={this.state._doorsbr} onChange={this.doorBrChange} />
                      <span className="sliderbc round"></span>
                    </label>
                  </td>
                  <td>
                    <label className="switch">
                      <input type="checkbox" checked={this.state._trunk} onChange={this.doorTrunkChange} />
                      <span className="sliderbc round"></span>
                    </label>
                  </td>
                  <td>
                    <label className="switch">
                      <input type="checkbox" checked={this.state._frunk} onChange={this.doorFrunkChange} />
                      <span className="sliderbc round"></span>
                    </label>
                  </td>
                </tr>
              </tbody>
            </table>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel style={{ backgroundColor: "hsl(275, 0%, 20%)" }}
          defaultExpanded={true}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon style={{ color: "hsl(0, 0%, 85%)" }} />}
            aria-controls="panel2a-content"
            id="panel2a-header"
            style={{ backgroundColor: "hsl(275, 0%, 20%)", color: "hsl(0, 0%, 85%)" }}>
            <div style={{ display: "flex", width: "100%", justifyContent: "space-between" }}>
              <p className="textBC">WINDOWS</p>
              {this.state._windows ? <Chip label="OPEN" onClick={this.windowscloseAll} className="textSmallBC" style={{ backgroundColor: "hsl(0, 100%, 39%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
              {!this.state._windows ? <Chip label="CLOSED" onClick={e => {e.stopPropagation()}} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
              <div></div>
            </div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <table style={{ width: "100%" }}>
              <tbody>
                <tr>
                  <td>
                    <p className="textSmallBC" style={{ paddingLeft: "1.3vw" }}>Front L</p>
                    <div style={{ paddingLeft: "1.3vw" }}>
                      {this.state._windowfl !== 100 ? <Chip label="CLOSED" onClick={this.windowclickclosefl} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                      {this.state._windowfl === 100 ? <Chip label="CLOSED" className="textSmallBC" style={{ backgroundColor: "green", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                    </div>
                  </td>
                  <td>
                    <p className="textSmallBC" style={{ paddingLeft: "1.3vw" }}>Front R</p>
                    <div style={{ paddingLeft: "1.3vw" }}>
                      {this.state._windowfr !== 100 ? <Chip label="CLOSED" onClick={this.windowclickclosefr} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                      {this.state._windowfr === 100 ? <Chip label="CLOSED" className="textSmallBC" style={{ backgroundColor: "green", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                    </div>
                  </td>
                  <td>
                    <p className="textSmallBC" style={{ paddingLeft: "1.3vw" }}>Back L</p>
                    <div style={{ paddingLeft: "1.3vw" }}>
                      {this.state._windowbl !== 100 ? <Chip label="CLOSED" onClick={this.windowclickclosebl} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                      {this.state._windowbl === 100 ? <Chip label="CLOSED" className="textSmallBC" style={{ backgroundColor: "green", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                    </div>
                  </td>
                  <td>
                    <p className="textSmallBC" style={{ paddingLeft: "1.3vw" }}>Back R</p>
                    <div style={{ paddingLeft: "1.3vw" }}>
                      {this.state._windowbr !== 100 ? <Chip label="CLOSED" onClick={this.windowclickclosebr} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                      {this.state._windowbr === 100 ? <Chip label="CLOSED" className="textSmallBC" style={{ backgroundColor: "green", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <Slider
                      orientation="vertical"
                      defaultValue={this.state._windowfl}
                      style={{ height: '6vh', marginTop: '5px', marginLeft: '20px' }}
                      value={this.state._windowfl}
                      onChange={this.windowflChange}
                    />
                  </td>
                  <td>
                    <Slider
                      orientation="vertical"
                      defaultValue={this.state._windowfr}
                      style={{ height: '6vh', marginTop: '5px', marginLeft: '20px' }}
                      value={this.state._windowfr}
                      onChange={this.windowfrChange}
                    />
                  </td>
                  <td>
                    <Slider
                      orientation="vertical"
                      defaultValue={this.state._windowbl}
                      style={{ height: '6vh', marginTop: '5px', marginLeft: '20px' }}
                      value={this.state._windowbl}
                      onChange={this.windowblChange}
                    />
                  </td>
                  <td>
                    <Slider
                      orientation="vertical"
                      defaultValue={this.state._windowbr}
                      style={{ height: '6vh', marginTop: '5px', marginLeft: '20px' }}
                      value={this.state._windowbr}
                      onChange={this.windowbrChange}
                    />
                  </td>
                </tr>
                <tr>
                  <td>
                    <div style={{ paddingLeft: "1vw" }}>
                      {this.state._windowfl === 100 ? <Chip label="OPEN" onClick={this.windowclickopenfl} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                      {this.state._windowfl !== 100 ? <Chip label="OPEN" className="textSmallBC" style={{ backgroundColor: "green", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                    </div>
                  </td>
                  <td>
                    <div style={{ paddingLeft: "1vw" }}>
                      {this.state._windowfr === 100 ? <Chip label="OPEN" onClick={this.windowclickopenfr} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                      {this.state._windowfr !== 100 ? <Chip label="OPEN" className="textSmallBC" style={{ backgroundColor: "green", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                    </div>
                  </td>
                  <td>
                    <div style={{ paddingLeft: "1vw" }}>
                      {this.state._windowbl === 100 ? <Chip label="OPEN" onClick={this.windowclickopenbl} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                      {this.state._windowbl !== 100 ? <Chip label="OPEN" className="textSmallBC" style={{ backgroundColor: "green", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                    </div>
                  </td>
                  <td>
                    <div style={{ paddingLeft: "1vw" }}>
                      {this.state._windowbr === 100 ? <Chip label="OPEN" onClick={this.windowclickopenbr} className="textSmallBC" style={{ backgroundColor: "hsl(275, 0%, 45%)", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                      {this.state._windowbr !== 100 ? <Chip label="OPEN" className="textSmallBC" style={{ backgroundColor: "green", color: "hsl(0, 0%, 85%)", margin: 0 }} /> : null}
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div >
    );
  }

  doorscloseAll = e => {
    e.stopPropagation();
    this.state._doors = false;
    this.state._doorsfl = false;
    this.state._doorsfr = false;
    this.state._doorsbl = false;
    this.state._doorsbr = false;
    this.state._trunk = false;
    this.state._frunk = false;
    this.setState({ _doorsfl: this.state._doorsfl, _doorsfr: this.state._doorsfr, _doorsbl: this.state._doorsbl, _doorsbr: this.state._doorsbr, _frunk: this.state._frunk, _trunk: this.state._trunk, _doors: this.state._doors });
  }

  doorFlChange = () => {
    this.state._doorsfl = !this.state._doorsfl;
    this.setState({ _doorsfl: this.state._doorsfl });
    this.checkAllDoors();
  }

  doorFrChange = () => {
    this.state._doorsfr = !this.state._doorsfr;
    this.setState({ _doorsfr: this.state._doorsfr });
    this.checkAllDoors();
  }

  doorBlChange = () => {
    this.state._doorsbl = !this.state._doorsbl;
    this.setState({ _doorsbl: this.state._doorsbl });
    this.checkAllDoors();
  }

  doorBrChange = () => {
    this.state._doorsbr = !this.state._doorsbr;
    this.setState({ _doorsbr: this.state._doorsbr });
    this.checkAllDoors();
  }

  doorFlChange = () => {
    this.state._doorsfl = !this.state._doorsfl;
    this.setState({ _doorsfl: this.state._doorsfl });
    this.checkAllDoors();
  }

  doorTrunkChange = () => {
    this.state._trunk = !this.state._trunk;
    this.setState({ _trunk: this.state._trunk });
    this.checkAllDoors();
  }

  doorFrunkChange = () => {
    this.state._frunk = !this.state._frunk;
    this.setState({ _frunk: this.state._frunk });
    this.checkAllDoors();
  }

  checkAllDoors = () => {
    if (this.state._doorsfl || this.state._doorsfr || this.state._doorsbl || this.state._doorsbr || this.state._frunk || this.state._trunk) {
      this.state._doors = true;
    } else {
      this.state._doors = false;
    }
    this.setState({ _doors: this.state._doors });
  }

  windowscloseAll = e => {
    e.stopPropagation();
    this.state._windows = false;
    this.state._windowfl = 100;
    this.state._windowfr = 100;
    this.state._windowbl = 100;
    this.state._windowbr = 100;
    this.setState({ _windowfl: this.state._windowfl, _windowfr: this.state._windowfr, _windowbl: this.state._windowbl, _windowbr: this.state._windowbr, _windows: this.state._windows });
  }

  windowclickclosefl = () => {
    this.state._windowfl = 100;
    this.setState({ _windowfl: this.state._windowfl })
    this.checkAllWindows()
  }

  windowclickclosefr = () => {
    this.state._windowfr = 100;
    this.setState({ _windowfr: this.state._windowfr })
    this.checkAllWindows()
  }

  windowclickclosebl = () => {
    this.state._windowbl = 100;
    this.setState({ _windowbl: this.state._windowbl })
    this.checkAllWindows()
  }

  windowclickclosebr = () => {
    this.state._windowbr = 100;
    this.setState({ _windowbr: this.state._windowbr })
    this.checkAllWindows()
  }

  windowclickopenfl = () => {
    this.state._windowfl = 0;
    this.setState({ _windowfl: this.state._windowfl })
    this.checkAllWindows()
  }

  windowclickopenfr = () => {
    this.state._windowfr = 0;
    this.setState({ _windowfr: this.state._windowfr })
    this.checkAllWindows()
  }

  windowclickopenbl = () => {
    this.state._windowbl = 0;
    this.setState({ _windowbl: this.state._windowbl })
    this.checkAllWindows()
  }

  windowclickopenbr = () => {
    this.state._windowbr = 0;
    this.setState({ _windowbr: this.state._windowbr })
    this.checkAllWindows()
  }

  windowflChange = (event, value) => {
    this.state._windowfl = value;
    this.setState({ _windowfl: this.state._windowfl })
    this.checkAllWindows();
  }

  windowfrChange = (event, value) => {
    this.state._windowfr = value;
    this.setState({ _windowfr: this.state._windowfr })
    this.checkAllWindows();
  }

  windowblChange = (event, value) => {
    this.state._windowbl = value;
    this.setState({ _windowbl: this.state._windowbl })
    this.checkAllWindows();
  }

  windowbrChange = (event, value) => {
    this.state._windowbr = value;
    this.setState({ _windowbr: this.state._windowbr })
    this.checkAllWindows();
  }

  checkAllWindows = () => {
    if (this.state._windowfl === 100 && this.state._windowfr === 100 && this.state._windowbl === 100 && this.state._windowbr === 100) {
      this.state._windows = false;
    } else {
      this.state._windows = true;
    }
    this.setState({ _windows: this.state._windows });
  }
}

