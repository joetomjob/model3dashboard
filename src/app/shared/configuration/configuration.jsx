import React, { Component } from 'react';
import './configuration-style.css'
import * as firebase from 'firebase';
export class ConfigurationComponent extends Component {
  state = {
    _model: 3,
    _color: 'blue',
    _sunroofString: 'Yes',
    _dualmotorString: 'Yes',
    _wheel: '21’’ grey turbines',
    _environment: 'ENINEERING',
    _nickname: 'LIGHTNING MCQUEEN',
    _vehicledatabaseid: 80,
    _vin: '5YJEF14NXJCA59714',

  }

  componentDidMount() {
    const rootRef = firebase.database().ref().child('configuration-data');
    rootRef.on('value', (snapshot) => {
      let items = snapshot.val();
      this.setState({
        _model: items[0].model,
        _color: items[0].color,
        _sunroofString: items[0].sunroof ? 'Yes' : 'No',
        _dualmotorString: items[0].dualmotor ? 'Yes' : 'No',
        _wheel: items[0].wheel
      })
    });

    const rootRef1 = firebase.database().ref().child('identity-data');
    rootRef1.on('value', (snapshot) => {
      let items = snapshot.val();
      this.setState({
        _environment: items[0].environment,
        _nickname: items[0].nickname,
        _vehicledatabaseid: items[0].vehicledatabaseid,
        _vin: items[0].vin
      })
    });
  }
  render() {
    return (
      <div className="configgrid">
        <div className="nickname" style={{ textAlign: "center", fontWeight: "bold" }}>
          <p className="text">{this.state._nickname}</p>
        </div>
        <div className="vinvalue" style={{ textAlign: "center", fontWeight: "bold" }}>
          <p className="text">{this.state._vin}</p>
        </div>
        <div className="dbidtext" style={{ textAlign: "right" }}>
          <p className="text">Database Id : </p>
        </div>
        <div className="dbidvalue">
          <p className="text">{this.state._vehicledatabaseid}</p>
        </div>
        <div className="modeltext" style={{ textAlign: "right" }}>
          <p className="text">Model : </p>
        </div>
        <div className="modelvalue">
          <p className="text">{this.state._model}</p>
        </div>
        <div className="carcolortext" style={{ textAlign: "right" }}>
          <p className="text">Color : </p>
        </div>
        <div className="carcolorvalue">
          <p className="text">{this.state._color}</p>
        </div>
        <div className="sunrooftext" style={{ textAlign: "right" }}>
          <p className="text">Sunroof : </p>
        </div>
        <div className="sunroofvalue">
          <p className="text"> {this.state._sunroofString} </p>
        </div>
        <div className="dualmotortext" style={{ textAlign: "right" }}>
          <p className="text">Dual Motor : </p>
        </div>
        <div className="dualmotorvalue">
          <p className="text"> {this.state._dualmotorString} </p>
        </div>
        <div className="envtext" style={{ textAlign: "right" }}>
          <p className="text">Environment : </p>
        </div>
        <div className="envvalue">
          <p className="text">{this.state._environment}</p>
        </div>
        <div className="wheeltypetext" style={{ textAlign: "right" }}>
          <p className="text">Wheel Type : </p>
        </div>
        <div className="wheeltypevalue">
          <p className="text">{this.state._wheel}</p>
        </div>
      </div>
    );
  }
}