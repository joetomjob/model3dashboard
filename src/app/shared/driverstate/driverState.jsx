import React, { Component } from 'react';
import network_check from '../../../assets/icons/network_check.svg'
import './driver-state-style.css'
import * as firebase from 'firebase';

export class DriverStateComponent extends Component {

  state = {
    _speed: 72,
    _acceleration: 4,
    _gear: 'D',
    counter: 0
  }

  componentDidMount() {
    const rootRef = firebase.database().ref().child('driverstate-data');
    rootRef.on('value', (snapshot) => {
      let items = snapshot.val();
      setInterval(() => {
        this.setState({
          _speed: items[this.state.counter].speed,
          _acceleration: items[this.state.counter].acceleration,
          _gear: items[this.state.counter].gear
        })
        if (this.state.counter === 1) {
          this.state.counter = 0;
        } else {
          this.state.counter++;
        }
        // console.log(items[0]);
      }, 250);
      this.setState({
      })
    });
  }

  render() {
    return (
      <div className="dscontrol">
        <div className="speed">
          <p className="text">
            <img src={network_check} alt="" />
            SPEED (mph)
          </p>
          <p className="textLarge">{this.state._speed}</p>
        </div>
        <div className="acceleration">
          <p className="textSmallDS">ACCELERATION(m/s<sup>2</sup>)</p>
          <p className="text">{this.state._acceleration}</p>
        </div>
        <div className="gear">
          <p className="textSmallDS">GEAR</p>
          <p className="text" style={{ fontWeight: "bold" }}>{this.state._gear}</p>
        </div>
        <div>
          <p className="textSmallDS">ODOMETER </p>
          <p className="textodo">2018 miles</p>
        </div>
      </div>
    );
  }
}