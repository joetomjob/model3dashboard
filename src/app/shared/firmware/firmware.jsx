import React, { Component } from 'react';
import './firmware-style.css'
import * as firebase from 'firebase';

export class FirmwareComponent extends Component {
  state = {
    _installedVersion: '1.2',
    _installingVersion: '1.3',
    _installingJobPercent: 0,
    counter: 0
  }

  componentDidMount() {
    const rootRef = firebase.database().ref().child('firmware-data');
    rootRef.on('value', (snapshot) => {
      let items = snapshot.val();
      setInterval(() => {
        this.setState({
          _installedVersion: items[this.state.counter].installedVersion,
          _installingVersion: items[this.state.counter].installingVersion,
          _installingJobPercent: Number(items[this.state.counter].installingJobProgress)
        })
        if (this.state.counter === 4) {
          this.state.counter = 0;
        } else {
          this.state.counter++;
        }
        // console.log(items[0]);
      }, 2000);
      this.setState({
      })
    });
  }


  render() {
    var installingJobPercent = this.state._installingJobPercent + '%';
    return (
      <div className="firmwarecontrol">
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <td>
                <div className="installed">
                  <p className="textSmallFW">INSTALLED VERSION</p>
                  <p className="text">{this.state._installedVersion}</p>
                </div>
              </td>
              <td>
                <div className="installing">
                  <p className="textSmallFW">INSTALLING VERSION</p>
                  <p className="text">{this.state._installingVersion}</p>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="installingbar">
          <div className="myProgress">
            <div className="myBar" style={{ width: installingJobPercent }}></div>
          </div>
        </div>
      </div>
    );
  }
}
