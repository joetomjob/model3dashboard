import React, { Component } from 'react';
import './hvac-style.css'
import acunit from '../../../assets/icons/ac_unit.svg'
import toys from '../../../assets/icons/toys.svg'
import * as firebase from 'firebase';

export class HvacComponent extends Component {
  state = {
    _ambienttemp: 67,
    _externaltemp: 38,
    _tempSlider: 65,
    _fanspeed: 3,
    _sunroof: false,
    _chargeport: false,
    _sunrooftext: 'CLOSED',
    _chargeporttext: 'CLOSED',
    counter: 0
  }

  componentDidMount() {
    const rootRef = firebase.database().ref().child('hvac-data');
    rootRef.on('value', (snapshot) => {
      let items = snapshot.val();
      setInterval(() => {
        this.setState({
          _ambienttemp: items[this.state.counter].ambientTemp,
          _externaltemp: items[this.state.counter].externalTemp,
          _tempSlider: items[this.state.counter].sliderTemp,
          _fanspeed: items[this.state.counter].fanspeed
        })
        if (this.state.counter === 1) {
          this.state.counter = 0;
        } else {
          this.state.counter++;
        }
        // console.log(items[0]);
      }, 10000);
      this.setState({
      })
    });
  }

  render() {
    return (
      <div className="hvaccontrol">
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <td>
                <div className="ambtemp">
                  <p className="textSmallHvac">AMBIENT TEMPERATURE</p>
                  <p className="text">{this.state._ambienttemp}°F</p>
                </div>
              </td>
              <td>
                <div className="exttemp">
                  <p className="textSmallHvac">EXTERNAL TEMPERATURE</p>
                  <p className="text">{this.state._externaltemp}°F</p>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="tempcontrol">
          <p className="textSmallHvac">
            <img src={acunit} alt="" />
            TEMPERATURE
          </p>
          <input type="range" min="32" max="90" className="slider" value={this.state._tempSlider} onChange={this.tempChange} />
          <p className="text">{this.state._tempSlider}°F</p>
        </div>
        <div className="fanspeed">
          <p className="textSmallHvac">
            <img src={toys} alt="" />
            FAN SPEED
          </p>
          <input type="range" min="1" max="5" className="slider" value={this.state._fanspeed} onChange={this.fanSpeedChange} />
          <p className="text">{this.state._fanspeed}</p>
        </div>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <td>
                <div className="sunrooft">
                  <p className="textSmallHvac">SUNROOF</p>
                </div>
              </td>
              <td>
                <div className="sunroofv">
                  <label className="switch">
                    <input type="checkbox" checked={this.state._sunroof} onChange={this.sunRoofChange} />
                    <span className="sliderc round"></span>
                  </label>
                </div>
              </td>
              <td>
                <div className="chargeporttext">
                  <p className="textSmallHvac">CHARGE PORT</p>
                </div>
              </td>
              <td>
                <div className="chargeportvalue">
                  <label className="switch">
                    <input type="checkbox" checked={this.state._chargeport} onChange={this.chargePortChange} />
                    <span className="sliderc round"></span>
                  </label>
                </div>
              </td>
            </tr>
            <tr>
              <td></td>
              <td className="textExtraSmall">
                {this.state._sunrooftext}
              </td>
              <td></td>
              <td className="textExtraSmall">
                {this.state._chargeporttext}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }

  tempChange = (event) => {
    this.state._tempSlider = event.target.value;
    this.setState({ _tempSlider: event.target.value });
  }

  fanSpeedChange = (event) => {
    this.state._fanspeed = event.target.value;
    this.setState({ _fanspeed: event.target.value });
  }

  sunRoofChange = () => {
    this.state._sunroof = !this.state._sunroof;
    if (this.state._sunroof) {
      this.state._sunrooftext = 'OPENED'
    } else {
      this.state._sunrooftext = 'CLOSED'
    }
    this.setState({ _sunroof: this.state._sunroof, _sunrooftext: this.state._sunrooftext });
  }

  chargePortChange = () => {
    this.state._chargeport = !this.state._chargeport;
    if (this.state._chargeport) {
      this.state._chargeporttext = 'OPENED'
    } else {
      this.state._chargeporttext = 'CLOSED'
    }
    this.setState({ _chargeport: this.state._chargeport, _chargeporttext: this.state._chargeporttext });
  }
}