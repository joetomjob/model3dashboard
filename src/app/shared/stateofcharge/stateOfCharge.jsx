import React, { Component } from 'react';
import './state-of-charge-style.css'
import battery_charging_full from '../../../assets/icons/battery_charging_full.svg'
import ev_station from '../../../assets/icons/ev_station.svg'
import * as firebase from 'firebase';

export class StateOfChargeComponent extends Component {

  state = {
    _percentRemaining: 80,
    _remainingMileage: 220,
    _mileagepercent: 70,
    _range: 310,
    counter: 0
  }

  componentDidMount() {
    const rootRef = firebase.database().ref().child('stateofcharge-data');
    rootRef.on('value', (snapshot) => {
      let items = snapshot.val();
      setInterval(() => {
        this.setState({
          _percentRemaining: items[this.state.counter].percentRemaining,
          _remainingMileage: items[this.state.counter].remainingMileage,
          _range: items[this.state.counter].range,
          _mileagepercent: (items[this.state.counter].remainingMileage / items[this.state.counter].range) * 100
        })
        if (this.state.counter === 2) {
          this.state.counter = 0;
        } else {
          this.state.counter++;
        }
        // console.log(items[0]);
      }, 5000);
      this.setState({
      })
    });
  }

  render() {
    var percentageMile = this.state._mileagepercent + '%';
    var percentCharge = this.state._percentRemaining + '%';
    return (
      <div className="stateofchargecontrol">
        <div style={{ padding: "2vh" }}>
          <p className="textSmallSC">CHARGE REMAINING
            <img src={battery_charging_full} alt="" />
          </p>
          <div className="battery">
            <div className={this.state._remainingMileage ? this.getPercentRemClass() : ''} style={{ width: percentCharge }}>
              {this.state._percentRemaining}%
            </div>
          </div>
        </div>
        <div style={{ padding: "2vh" }}>
          <p className="textSmallSC">MILEAGE REMAINING
              <img src={ev_station} alt="" />
          </p>
          <div className="battery">
            <div className={this.state._mileagepercent ? this.getRemMileageClass() : ''} style={{ width: percentageMile }}>
              {this.state._remainingMileage}miles
            </div>
          </div>
        </div>
      </div>
    );
  }

  getRemMileageClass() {
    if (this.state._mileagepercent > 50) {
      return 'battery-level-high';
    } else if (this.state._mileagepercent > 25) {
      return 'battery-level-medium';
    } else {
      return 'battery-level-low';
    }
  }

  getPercentRemClass() {
    if (this.state._percentRemaining > 50) {
      return 'battery-level-high';
    } else if (this.state._percentRemaining > 25) {
      return 'battery-level-medium';
    } else {
      return 'battery-level-low';
    }
  }
}