import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Home } from '././app/home/home';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyBDtBds4d1j-ncGBR0nhGJN_UaFq92umTI",
    authDomain: "model3dashboard.firebaseapp.com",
    databaseURL: "https://model3dashboard.firebaseio.com",
    projectId: "model3dashboard",
    storageBucket: "",
    messagingSenderId: "1002146122652",
    appId: "1:1002146122652:web:a604ab64e3c83668be1659"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

ReactDOM.render(<Home />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
